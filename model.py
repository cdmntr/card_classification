from keras import optimizers
from keras.models import Model
from keras.layers import Input, Dropout, Flatten, Dense, Conv2D
from keras.layers.convolutional import MaxPooling2D

# Model parameters

width, height = 35, 44
batch_size = 10
num_filter_a = 8
num_filter_b = 16
conv_size_a = 3
conv_size_b = 2
max_pool_size = 2
num_decks = 4
num_cards = 13
lr_deck = 0.001
lr_card = 0.001

def getModels():
	# Structure for deck categorization
	inputs_deck = Input(shape=(width, height, 3))
	conv_deck = Conv2D(num_filter_a, (conv_size_a, conv_size_a), padding='same', activation='relu')(inputs_deck)
	max_pool_deck = MaxPooling2D((max_pool_size, max_pool_size))(conv_deck)
	flatten_deck = Flatten()(max_pool_deck)
	dense_deck = Dense(256, activation='relu')(flatten_deck)
	dropout_deck = Dropout(0.5)(dense_deck)
	out_deck = Dense(num_decks, activation='softmax')(dropout_deck)

	model_deck = Model(inputs_deck, out_deck)

	model_deck.compile(
		loss = 'categorical_crossentropy',
		optimizer = optimizers.RMSprop(lr=lr_deck),
		metrics = ['accuracy']
	)
	
	# Structure for card number categorization
	inputs_card = Input(shape=(width, height, 3))
	conv_card = Conv2D(num_filter_a, (conv_size_a, conv_size_a), padding='same', activation='relu')(inputs_card)
	max_pool_card = MaxPooling2D((max_pool_size, max_pool_size))(conv_card)
	flatten_card = Flatten()(max_pool_card)
	dense_card = Dense(256, activation='relu')(flatten_card)
	dropout_card = Dropout(0.5)(dense_card)
	out_card = Dense(num_cards, activation='softmax')(dropout_card)

	# model = Model(inputs, [out_deck, out_card])
	model_card = Model(inputs_card, out_card)

	model_card.compile(
		loss = 'categorical_crossentropy',
		optimizer = optimizers.RMSprop(lr=lr_card),
		metrics = ['accuracy']
	)

	return {'deck': model_deck, 'card': model_card}