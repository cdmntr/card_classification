# Readme

## Files
1. model.py

	Contains neural network definition for number as well as deck predictor. It also initialises some variables used in model training and creation, as well as image dimensions.

2. run.py

	Driver code that initialises the models, prepares training data, creates necessary training and model directories essential for Keras and kicks off the training process. It also pickles the trained model structures, their weights and the class mapping used for prediction.

3. predict.py

	Helper code to output learned predictions by using a pre-trained model

## Workflow

```python run.py [--deck path] [--cards path] [--deck_test path] [--card_test path]```

- deck

	Specify path to folder where temporary images related to deck classification will be stored

- cards

	Specify path to folder where temporary images related to card classification will be stored

- deck_test

	Specify path to folder where testing data will be used for deck model evaluation

- card_test

	Specify path to folder where testing data will be used for card model evaluation

```python predict.py [-inp path] [-c_mod path] [-d_mod path] [-c_map path] [-d_map path]```

- inp

	Specify path to image or folder which you want the trained model to evaluate. Required.

- c_mod

	Specify path to trained cards model

- d_mod

	Specify path to trained decks model

- c_map

	Specify path to pickled cards mapping file

- d_map

	Specify path to pickled decks mapping file