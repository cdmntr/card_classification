import argparse, keras, os, pickle
from model import lr_card, lr_deck, width, height
import numpy as np
from scipy import misc
from keras import optimizers

parser = argparse.ArgumentParser()
parser.add_argument('-inp', '--input_data', help='Absolute or relative path of file/folder containing data to be predicted')
parser.add_argument('-c_mod', '--card_model', default='./models/card.h5', help='Absolute or relative path of card model to be used')
parser.add_argument('-d_mod', '--deck_model', default='./models/deck.h5', help='Absolute or relative path of deck model to be used')
parser.add_argument('-c_map', '--card_mapping', default='./models/card_class_indices.pickle', help='Absolute or relative path of card class mapping')
parser.add_argument('-d_map', '--deck_mapping', default='./models/deck_class_indices.pickle', help='Absolute or relative path of deck class mapping')
args = parser.parse_args()

'''
This program requires the model to be saved
since it loads a model for prediction.
If you want to create a model, please execute run.py
'''

try:
	card = keras.models.load_model(args.card_model)
	deck = keras.models.load_model(args.deck_model)

	card.compile(
		loss = 'categorical_crossentropy',
		optimizer = optimizers.RMSprop(lr=lr_card),
		metrics = ['accuracy']
	)
	deck.compile(
		loss = 'categorical_crossentropy',
		optimizer = optimizers.RMSprop(lr=lr_deck),
		metrics = ['accuracy']
	)
except:
	print('Unable to load both models. Please check path. Have you executed\
	 		run.py atleast once?')

try:
	with open(args.card_mapping, 'rb') as fin:
		card_map = pickle.load(fin)
	card_map = {v:k for k, v in card_map.items()}
	
	with open(args.deck_mapping, 'rb') as fin:
		deck_map = pickle.load(fin)
	deck_map = {v:k for k, v in deck_map.items()}
except:
	print('Could not load card and/or deck class mapping. Execute run.py\
			to make these')

def getPrediction(i, card_model, deck_model):
	img = misc.imread(i)[:,:,:3]
	img = misc.imresize(img, (width, height, 3), interp='lanczos')
	img = np.expand_dims(img, axis=0)
	c = card_model.predict(img, batch_size=1)
	d = deck_model.predict(img, batch_size=1)
	return '{}: {} {}'.format(
				i,
				card_map[np.argmax(c)],
				deck_map[np.argmax(d)]
			)

if os.path.exists(args.input_data):
	if os.path.isdir(args.input_data):
		for i in os.listdir(args.input_data):
			try:
				print(
					getPrediction(
						os.path.join(args.input_data, i),
						card,
						deck
					)
				)
			except:
				print('Error in %s' % i)
	else:
		try:
			print(
				getPrediction(
					args.input_data,
					card,
					deck
				)
			)
		except:
			print('Error in %s' % args.input_data)
else:
	print('Input data path not found')