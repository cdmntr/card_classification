import argparse, keras, os, pickle
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from keras import callbacks
import model, h5py

# Setting up argument parser
# python run.py --deck ./decks --card ./cards --deck_test ./fay/decks --card_test ./fay/cards

parser = argparse.ArgumentParser()
parser.add_argument('--deck', default='./decks', help='Absolute or relative path of folder containing training data for decks')
parser.add_argument('--card', default='./cards', help='Absolute or relative path of folder containing training data for cards')
parser.add_argument('--deck_test', edfault='./fay/decks', help='Absolute or relative path of folder containing deck test data')
parser.add_argument('--card_test', default='./fay/cards', help='Absolute or relative path of folder containing card test data')
args = parser.parse_args()

try:
	card = keras.models.load_model('models/card.h5')
	deck = keras.models.load_model('models/deck.h5')

except:
	if not os.path.exists('deck_gen'):
		os.mkdir('deck_gen')

	if not os.path.exists('card_gen'):
		os.mkdir('card_gen')

	data_deck_gen = ImageDataGenerator(
		1./255,
		samplewise_center=True,
		rotation_range= 90.0,
		fill_mode='constant',
		cval=0,
		horizontal_flip=True
	).flow_from_directory(
		args.deck,
		target_size=(model.width, model.height),
		class_mode='categorical',
		seed=42,
		save_to_dir='deck_gen',
		save_prefix='deck_'
	)

	data_card_gen = ImageDataGenerator(
		1./255,
		samplewise_center=True,
		rotation_range= 90.0,
		fill_mode='constant',
		cval=0,
		horizontal_flip=True
	).flow_from_directory(
		args.card,
		target_size=(model.width, model.height),
		class_mode='categorical',
		seed=42,
		save_to_dir='card_gen',
		save_prefix='card_'
	)

	models = model.getModels()

	deck = models['deck']
	card = models['card']

	deck.fit_generator(
		data_deck_gen,
		steps_per_epoch=50,
		epochs=10
	)

	card.fit_generator(
		data_card_gen,
		steps_per_epoch=100,
		epochs=100
	)

	target_dir = './models/'
	if not os.path.exists(target_dir):
		os.mkdir(target_dir)

	deck.save('./models/deck.h5')
	deck.save_weights('./models/deck_weights.h5')

	card.save('./models/card.h5')
	card.save_weights('./models/card_weights.h5')

finally:
	log_dir = './tf-log/'
	if not os.path.exists(log_dir):
		os.mkdir(log_dir)
	tb_cb = callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
	cbks = [tb_cb]

	data_deck_gen_test = ImageDataGenerator(
		fill_mode='constant',
		cval=0
	).flow_from_directory(
		args.deck_test,
		target_size=(model.width, model.height),
		class_mode='categorical',
		seed=42
	)

	data_card_gen_test = ImageDataGenerator(
		fill_mode='constant',
		cval=0
	).flow_from_directory(
		args.card_test,
		target_size=(model.width, model.height),
		class_mode='categorical',
		seed=42,
		batch_size=1
	)

	filenames_deck = data_card_gen_test.filenames
	n_samp_deck = len(filenames_deck)

	deck_predictions = deck.predict_generator(data_deck_gen_test, steps=n_samp_deck)

	filenames_card = data_card_gen_test.filenames
	n_samp_card = len(filenames_card)

	card_predictions = card.predict_generator(data_card_gen_test, steps=n_samp_card)

	try:
		print(data_deck_gen_test.class_indices)
		print(data_card_gen_test.class_indices)

		with open('models/deck_class_indices.pickle', 'wb') as fout:
			pickle.dump(data_deck_gen_test.class_indices, fout, pickle.HIGHEST_PROTOCOL)

		with open('models/card_class_indices.pickle', 'wb') as fout:
			pickle.dump(data_card_gen_test.class_indices, fout, pickle.HIGHEST_PROTOCOL)
	except:
		print("error in class indices")
	finally:
		# results_deck = deck.evaluate_generator(
		# 	data_deck_gen_test,
		# 	steps=300
		# )

		# results_card = card.evaluate_generator(
		# 	data_card_gen_test,
		# 	steps=300
		# )

		# print('Deck Prediction Loss: %f\nDeck Prediction Accuracy: %f' % (results_deck[0], results_deck[1]*100))
		# print('Number Prediction Loss: %f\nNumber Prediction Accuracy: %f' % (results_card[0], results_card[1]*100))

		with open('models/deck_pred.pickle', 'wb') as fout:
			pickle.dump(deck_predictions, fout, pickle.HIGHEST_PROTOCOL)

		with open('models/card_pred.pickle', 'wb') as fout:
			pickle.dump(card_predictions, fout, pickle.HIGHEST_PROTOCOL)